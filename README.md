# Desafio Juntos Somos+

O serviço crawller recebe os dados do arquivo CSV e da requisição http em JSON. Trata esses dados e os converte em novo JSON que por fim é armazenado no banco de dados.

Foi utilizado o banco de dados MongoDB para armazenar os dados dos usuários.

A aplicação deve ser iniciada no Docker usando o comando:

- node src/crawler/src/indexCSV.js

- node src/crawler/src/IndexJSON.js

- docker-compose up -d

O Docker instala as bibliotecas e inicia a API.
A porta para a requisição é:

http://localhost:3000/user/1

Sendo 1 o numero da pagina desejada.
