const clienteType = require('../services/type');
const region = require('../services/stateRegion');


function jsonschemeCSV(indexs, datas) {

    return {
        "type": clienteType(datas[indexs].location__coordinates__latitude, datas[indexs].location__coordinates__longitude),
        "gender": datas[indexs].gender,
        "name": {
            "title": datas[indexs].name__title,
            "first": datas[indexs].name__first,
            "last": datas[indexs].name__last
        },
        "location": {
            "region": region(datas[indexs].location__state),
            "street": datas[indexs].location__street,
            "city": datas[indexs].location__citindexs,
            "state": datas[indexs].location__state,
            "postcode": datas[indexs].location__postcode,
            "coordinates": {
                "latitude": datas[indexs].location__coordinates__latitude,
                "longitude": datas[indexs].location__coordinates__longitude
            },
            "timezone": {
                "offset": datas[indexs].location__timezone__offset,
                "description": datas[indexs].location__timezone__description
            }
        },
        "email": datas[indexs].email,
        "birthday": datas[indexs].dob__date,
        "registered": datas[indexs].registered__date,
        "telephoneNumbers": [
            datas[indexs].phone
        ],
        "mobileNumbers": [
            datas[indexs].cell
        ],
        "picture": {
            "large": datas[indexs].picture__large,
            "medium": datas[indexs].picture__medium,
            "thumbnail": datas[indexs].picture__thumbnail
        },
        "nationality": "BR"
    }
}

module.exports = jsonschemeCSV