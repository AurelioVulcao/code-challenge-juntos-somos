const region = require('../services/stateRegion');
const clienteType = require('../services/type');

function jsonscheme(indexs, datas) {

    return {
        "type": clienteType(datas[indexs].location.coordinates.latitude,
            datas[indexs].location.coordinates.longitude),
        "gender": datas[indexs].gender,
        "name": {
            "title": datas[indexs].name.title,
            "first": datas[indexs].name.first,
            "last": datas[indexs].name.last
         },
        "location": {
            "region": region(datas[indexs].location.state),
            "street": datas[indexs].location.street,
            "city": datas[indexs].location.city,
            "state": datas[indexs].location.state,
            "postcode": datas[indexs].location.state,
            "coordinates": {
                "latitude": datas[indexs].location.coordinates.latitude,
                "longitude": datas[indexs].location.coordinates.longitude
            },
            "timezone": {
                "offset": datas[indexs].location.timezone.offset,
                "description": datas[indexs].location.timezone.description
            }
        },
        "email": datas[indexs].email,
        "birthday": datas[indexs].dob.date,
        "registered": datas[indexs].registered.date,
        "telephoneNumbers": [
            datas[indexs].phone
        ],
        "mobileNumbers": [
            datas[indexs].cell
        ],
        "picture": {
            "large": datas[indexs].picture.large,
            "medium": datas[indexs].picture.medium,
            "thumbnail": datas[indexs].picture.thumbnail
        },
        "nationality": "BR"
    }
}

module.exports = jsonscheme