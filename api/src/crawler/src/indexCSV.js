const csv = require('csvtojson');
const region = require('./services/stateRegion');
const clienteType = require('./services/type');
var save = require("./services/mongoDB");
const jsonschemeCSV = require('./models/shemecsv')
const treating = require('./services/treating');


// Importanto o arquivo CSV para tratamento
const csvFilePath = 'input-backend.csv'


// data são os dados as serem exportados para o banco de dados
const data = []

// Função de tratamento do CSV
csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {
        // console.log(jsonObj[0]);
        // data1.push(jsonObj)
        const data1 = jsonObj
        // console.log(data1);
        
       
        for (i in jsonObj) {
            treating(i, data1)
        }
        

        for (i in jsonObj) {
            data.push(
                jsonschemeCSV(i, data1)
            )
        }
        // console.log(data[0]);
        // salvando no banco de dados
        save(data)

    })

