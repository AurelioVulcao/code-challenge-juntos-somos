function clienteType(x, y) {
    // latitudes e longitude

    if (x >= -54.777426 && x <= -46.603598 && y >= -26.155681 && y <= -34.016466) {
        return 'normal'
    }
    if (x >= -52.997614 && x <= -44.428305 && y >= -19.766959 && y <= -23.966413) {
        return 'especial'
    }
    if (x >= -46.361899 && x <= -34.276938 && y >= -2.196998 && y <= -15.411580) {
        return 'especial'
    }

    else {
        return 'laborious'
    }
}

module.exports = clienteType


// ESPECIAL
// minlon: -2.196998
// minlat -46.361899
// maxlon: -15.411580
// maxlat: -34.276938

// minlon: -19.766959
// minlat -52.997614
// maxlon: -23.966413
// maxlat: -44.428305
// NORMAL
// minlon: -26.155681
// minlat -54.777426
// maxlon: -34.016466
// maxlat: -46.603598