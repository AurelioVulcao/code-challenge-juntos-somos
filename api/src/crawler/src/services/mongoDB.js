const mongoose = require("mongoose");
const localidadesSchema = require('../models/mongodb');



// local onde o banco está sendo salvo
const URL =
  "mongodb+srv://admin:1234@cluster0-9jhwf.mongodb.net/xx?retryWrites=true&w=majority";

// preferi inserir os dados como string para não ser necessario tratar "." e ","


module.exports = function (items) {
  var localidadeModel = mongoose.model(
    "datas",
    localidadesSchema
  );

  //var caSe = cases;
  var localidades = [];
  items.forEach(function (item, index) {
    var localidade = new localidadeModel();

    localidades.push(item);
  });


  mongoose.connect(
    URL,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    },
    function (error) {
      if (!error) {

        localidadeModel
          .insertMany(localidades)
          .then(function (docs) {
            console.log("salvo com sucesso");

            mongoose.disconnect();
          })
          .catch(function (error) {
            console.log(error);
            process.exit(2);
          });
      } else {
        console.log(error);
        process.exit(1);
      }
    }
  );
};
