const mongoose = require("mongoose");


const localidadesSchema = new mongoose.Schema({
    "type": {
        type: String
    },
    "gender": {
        type: String
    },
    "name": {
        "title": {
            type: String
        },
        "first": {
            type: String
        },
        "last": {
            type: String
        }
    },
    "location": {
        "region": {
            type: String
        },
        "street": {
            type: String
        },
        "city": {
            type: String
        },
        "state": {
            type: String
        },
        "postcode": {
            type: String
        },
        "coordinates": {
            "latitude": {
                type: String
            },
            "longitude": {
                type: String
            }
        },
        "timezone": {
            "offset": {
                type: String
            },
            "description": {
                type: String
            }
        }
    },
    "email": {
        type: String
    },
    "birthday": {
        type: String
    },
    "registered": {
        type: String
    },
    "telephoneNumbers": [
        {
            type: String
        }
    ],
    "mobileNumbers": [
        {
            type: String
        }
    ],
    "picture": {
        "large": {
            type: String
        },
        "medium": {
            type: String
        },
        "thumbnail": {
            type: String
        }
    },
    "nationality": {
        type: String
    }
});

module.exports = localidadesSchema