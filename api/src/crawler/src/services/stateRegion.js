// Tratar região do estado
function region(y){
    // sul
    if (y == 'santa catarina') {
        return 'sul'
    }
    if (y == 'parana') {
        return 'sul'
    }
    if (y == 'rio grande do sul') {
        return 'sul'
    }
    // sudeste
    if (y == 'são paulo') {
        return 'sudeste'
    }
    if (y == 'rio de janeiro') {
        return 'sudeste'
    }
    if (y == 'espirito santo') {
        return 'sudeste'
    }
    if (y == 'minas gerais') {
        return 'sudeste'
    }
    // centro-oeste
    if (y == 'distrito federal') {
        return 'centro-oeste'
    }
    if (y == 'goiáis') {
        return 'centro-oeste'
    }
    if (y == 'mato grosso') {
        return 'centro-oeste'
    }
    if (y == 'mato grosso do sul') {
        return 'centro-oeste'
    }
    // nordeste
    if (y == 'bahia') {
        return 'nordeste'
    }
    if (y == 'sergipe') {
        return 'nordeste'
    }
    if (y == 'alagoas') {
        return 'nordeste'
    }
    if (y == 'pernanbuco') {
        return 'nordeste'
    }
    if (y == 'paraiba') {
        return 'nordeste'
    }
    if (y == 'rio grande do norte') {
        return 'nordeste'
    }
    if (y == 'ceará') {
        return 'nordeste'
    }
    if (y == 'piaui') {
        return 'nordeste'
    }
    if (y == 'maranhão') {
        return 'nordeste'
    }
    // norte
    if (y == 'tocantins') {
        return 'norte'
    }
    if (y == 'pará') {
        return 'norte'
    }
    if (y == 'amapá') {
        return 'norte'
    }
    if (y == 'roraima') {
        return 'norte'
    }
    if (y == 'amazonas') {
        return 'norte'
    }
    if (y == 'rondônia') {
        return 'norte'
    }
    if (y == 'acre') {
        return 'norte'
    }
    else{
        return 'undefined'
    }
}


module.exports = region